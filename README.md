# DevOps Assignment

## Requirements

- Docker
- Run `docker swarm init` or `docker swarm join` command to initialize a swarm

## Deployment of stack

Run:
`./deploy.sh`

Check status of deployment using `docker stack services els` command.

Testing examples:

`wget http://localhost:8000/airports/NL`

`wget http://localhost:8000/countries/NL`

`wget http://localhost:8000/countries`

`wget http://localhost:8000/airports`

## Updating stack

Run for performing rolling update:
`./update.sh`

Check status of updating using `docker stack services els` command.

Testing examples:

`wget http://localhost:8000/airports?full=0`

`wget http://localhost:8000/airports/EHAM`

`wget http://localhost:8000/search/NL`

## Removing stack

Run:
`./remove.sh`

## Technologies

- Docker
- nginx
- Shell

## Additional information

`/health/ready` endpoint is bugged for `airports-assembly-1.0.1.jar` and for `airports-assembly-1.1.0.jar`