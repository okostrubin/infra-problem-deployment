#!/bin/sh

set -e

#read variables
while IFS="=" read -r name value || [[ -n $name ]]
    do declare "$name=${value}"
done < .env

docker build -t nginx-els:$NGINX_ELS_VERSION --build-arg "NGINX_ELS_VERSION=$NGINX_ELS_VERSION" ./nginx-els
docker build -t countries-assembly:$COUNTRIES_ASSEMBLY_VERSION --build-arg "COUNTRIES_ASSEMBLY_VERSION=$COUNTRIES_ASSEMBLY_VERSION" ./countries-assembly
docker build -t airports-assembly:$AIRPORTS_ASSEMBLY_VERSION --build-arg "AIRPORTS_ASSEMBLY_VERSION=$AIRPORTS_ASSEMBLY_VERSION" ./airports-assembly

env $(cat .env | grep ^[A-Z] | xargs) docker stack deploy -c els.yml els